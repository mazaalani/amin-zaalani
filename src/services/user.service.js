import { API } from "constants/api";

export const userService = {
  list,
};

function list(id) {
  const getInfo = { method: "GET" };
  if (id) {
    return new Promise((resolve, reject) => {
      fetch(`${API}/users/${id}`, getInfo)
        .then((result) => {
          return result.json();
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  } else {
    return new Promise((resolve, reject) => {
      fetch(`${API}/users/`, getInfo)
        .then((result) => {
          return result.json();
        })
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
