import React, { Component, Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";

import { title, history } from "utils";

import { userService } from "services";

import { UserInfo, UserArticles } from "components";

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      id: null,
      user: {},
    };
  }

  componentDidMount() {
    // userService.list() ... Pour remplir this.state.list
    userService.list().then((users) => {
      //enleve les char speciaux des noms d'usagers
      users.forEach((user) => {
        user.name = user.name.replace(/[^\w\s]/gi, "");
      });
      this.setState({ list: users });
    });
    //maj url au rechargement
    history.push("/users/");
    //fetch user au chargenment initial
    this.fetchUser();
  }

  //maj valeur id avec celle de la select
  changeUrl = (e) => {
    this.setState({ id: parseInt(e.target.value) });
    //maj url
    let url = "/users/" + e.target.value;
    history.push(url);
    this.fetchUser();
  };

  //valeur du id de la requete a partir de l'url
  getReqId = () => {
    let locPath = history.location.pathname,
      locPathVal;
    locPathVal = locPath.substring(locPath.lastIndexOf("/") + 1);
    return locPathVal;
  };

  fetchUser = () => {
    let reqId = 1;
    if (this.getReqId()) reqId = this.getReqId();
    //fetch user
    userService.list(reqId).then((userDetails) => {
      this.setState({ user: userDetails });
    });
  };

  render() {
    return (
      <Fragment>
        <Helmet>{title("Page secondaire")}</Helmet>

        <div className="user-page content-wrap">
          <Link to="/" className="nav-arrow">
            <Icon style={{ transform: "rotate(180deg)" }}>arrow_right_alt</Icon>
          </Link>

          <div className="users-select">
            <h1>
              <select onChange={this.changeUrl}>
                {
                  /* Liste dynamique à partir de l'API */
                  this.state.list.map((user) => {
                    return (
                      <option key={`${user.id}`} value={`${user.id}`}>
                        {user.name} #{user.id}
                      </option>
                    );
                  })
                }
              </select>
            </h1>
          </div>

          <div className="infos-block">
            {
              /* Infos dynamiques sur l'utilisateur sélectionné */
              <UserInfo user={this.state.user} />
            }
          </div>

          <div className="articles-list">
            {
              /* Liste dynamique tirée de l'utilisateur sélectionné */
              <UserArticles
                userArticles={this.state.user.articles}
              ></UserArticles>
            }
          </div>
        </div>
      </Fragment>
    );
  }
}

export default UserPage;
