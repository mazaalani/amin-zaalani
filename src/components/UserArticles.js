import React, { Component } from "react";

import { userService } from "services";

export class UserArticles extends Component {
  render() {
    let articles = this.props.userArticles;
    if (articles)
      return articles.map((article) => (
        <div key={article.id}>
          <h3>{article.name}</h3>
          <p>{article.content}</p>
        </div>
      ));
    else return <div></div>;
  }
}

export default UserArticles;
