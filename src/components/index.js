// Exemple pour rendre un component disponible :
// import Component from './Component';
// export { Component }
import UserInfo from "./UserInfo";
import UserArticles from "./UserArticles";

export { UserInfo, UserArticles };

// Dans les autres fichiers :
// import { Component } from 'components';
