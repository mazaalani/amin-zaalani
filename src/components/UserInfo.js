import React, { Component } from "react";

export class UserInfo extends Component {
  render() {
    return (
      <div>
        <p>
          Occupation: <span> {this.props.user.occupation} </span>
        </p>
        <p>
          Date de naissance: <span> {this.props.user.birthdate} </span>
        </p>
      </div>
    );
  }
}

export default UserInfo;
